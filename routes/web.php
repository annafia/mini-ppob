<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PulsaController;
use App\Http\Controllers\PacketDataController;
use App\Http\Controllers\PLNPrabayarController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () { return view('index'); });

Route::get('/login', function () { return view('login'); })->name('login');

Route::get('/pulsa', [PulsaController::class, 'view'])->name('pulsa');
Route::post('/pulsa', [PulsaController::class, 'add'])->name('pulsa');

Route::get('/packet-data', [PacketDataController::class, 'view'])->name('packet-data');
Route::post('/packet-data', [PacketDataController::class, 'add'])->name('packet-data');

Route::get('/pln', [PLNPrabayarController::class, 'view'])->name('pln');
Route::post('/pln', [PLNPrabayarController::class, 'add'])->name('pln');

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');
