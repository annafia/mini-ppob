<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MPacketData extends Model
{
    use HasFactory;

    protected $fillable = [
        'packet_name',
        'packet_price',
    ];
}
