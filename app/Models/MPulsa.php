<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MPulsa extends Model
{
    use HasFactory;
    protected $fillable = [
        'nominal',
        'price',
    ];
}
