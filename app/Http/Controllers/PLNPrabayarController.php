<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PLNPrabayar;
use App\Models\MPLNPrabayar;

class PLNPrabayarController extends Controller
{
    public function view() {
        $m_pln = MPLNPrabayar::orderBy('id')->get();
        return view('pln', compact('m_pln'));
    }
    public function add(Request $request) {
        $request->validate([
            'customer_id' => 'required|numeric',
            'nominal_id' => 'required|numeric',
        ]);

        $nominal = MPLNPrabayar::where('id', $request->nominal_id)->first();
        if (!$nominal) {
            return back()->withErrors(['Packet is not found. Please refresh your browser.']);
        }

        $pln = new PLNPrabayar();
        $pln->customer_id = $request->customer_id;
        $pln->nominal_id = $request->nominal_id;
        $pln->price = $nominal->nominal;
        $pln->save();

        return back()->with('pln_purchased', 'PLN Prabayar has been purchased sucessfully.');
    }
}
