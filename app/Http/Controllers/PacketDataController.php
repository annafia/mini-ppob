<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MPacketData;
use App\Models\PacketData;

class PacketDataController extends Controller
{
    public function view() {
        $m_packet_data = MPacketData::orderBy('id')->get();
        return view('packet-data', compact('m_packet_data'));
    }

    public function add(Request $request) {
        $request->validate([
            'phone' => 'required|numeric',
            'packet_id' => 'required|numeric',
        ]);

        $nominal = MPacketData::where('id', $request->packet_id)->get();
        if (!$nominal) {
            return back()->withErrors(['Packet is not found. Please refresh your browser.']);
        }

        $packet_data = new PacketData();
        $packet_data->phone = $request->phone;
        $packet_data->packet_id = $request->packet_id;
        $packet_data->price = $nominal->first()->packet_price;
        $packet_data->save();

        return back()->with('packet_data_purchased', 'Paket Data has been purchased sucessfully.');
    }
}
