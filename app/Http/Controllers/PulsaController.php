<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MPulsa;
use App\Models\Pulsa;

class PulsaController extends Controller
{
    public function view() {
        $m_pulsa = MPulsa::orderBy('nominal')->get();
        return view('pulsa', compact('m_pulsa'));
    }

    public function add(Request $request) {
        $request->validate([
            'phone' => 'required|numeric',
            'nominal_id' => 'required|numeric',
        ]);

        $nominal = MPulsa::where('id', $request->nominal_id)->get();
        if (!$nominal) {
            return back()->withErrors(['Packet is not found. Please refresh your browser.']);
        }

        $pulsa = new Pulsa();
        $pulsa->phone = $request->phone;
        $pulsa->nominal_id = $request->nominal_id;
        $pulsa->price = $nominal->first()->price;
        $pulsa->save();

        return back()->with('pulsa_purchased', 'Pulsa has been purchased sucessfully.');
    }
}
