-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 03, 2020 at 11:56 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mini-ppob`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2014_10_12_200000_add_two_factor_columns_to_users_table', 1),
(4, '2019_08_19_000000_create_failed_jobs_table', 1),
(5, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(6, '2020_10_02_190524_create_sessions_table', 1),
(11, '2020_10_03_063056_create_pulsas_table', 2),
(12, '2020_10_03_064116_create_m_pulsas_table', 2),
(13, '2020_10_03_082216_create_packet_data_table', 3),
(14, '2020_10_03_082227_create_m_packet_data_table', 3),
(17, '2020_10_03_085209_create_m_p_l_n_prabayars_table', 4),
(19, '2020_10_03_085221_create_p_l_n_prabayars_table', 5);

-- --------------------------------------------------------

--
-- Table structure for table `m_packet_data`
--

CREATE TABLE `m_packet_data` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `packet_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `packet_price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `m_packet_data`
--

INSERT INTO `m_packet_data` (`id`, `packet_name`, `packet_price`, `created_at`, `updated_at`) VALUES
(1, 'Internet OMG 150RB / 30 Hari', '150000', NULL, NULL),
(2, 'Data 2GB', '25000', NULL, NULL),
(3, 'Data 12GB', '100000', NULL, NULL),
(4, 'Data 50GB', '200000', NULL, NULL),
(5, 'Broadband Single Zone 6.5GB', '65000', NULL, NULL),
(6, 'Broadband Single Zone 10GB', '85000', NULL, NULL),
(7, 'Internet Combo 10GB', '100000', NULL, NULL),
(8, 'Internet OMG 30RB / 7 Hari', '30000', NULL, NULL),
(9, 'Internet OMG 75RB / 30 Hari', '75000', NULL, NULL),
(10, 'Internet OMG 100RB / 30 Hari', '100000', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `m_pulsas`
--

CREATE TABLE `m_pulsas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nominal` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `m_pulsas`
--

INSERT INTO `m_pulsas` (`id`, `nominal`, `price`, `created_at`, `updated_at`) VALUES
(1, 15000, 16000, NULL, NULL),
(2, 25000, 25500, NULL, NULL),
(3, 75000, 75000, NULL, NULL),
(4, 100000, 100000, NULL, NULL),
(5, 150000, 150000, NULL, NULL),
(6, 200000, 200000, NULL, NULL),
(7, 300000, 300000, NULL, NULL),
(8, 500000, 500000, NULL, NULL),
(9, 30000, 30000, NULL, NULL),
(10, 50000, 50000, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `m_p_l_n_prabayars`
--

CREATE TABLE `m_p_l_n_prabayars` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nominal` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `m_p_l_n_prabayars`
--

INSERT INTO `m_p_l_n_prabayars` (`id`, `nominal`, `created_at`, `updated_at`) VALUES
(1, 15000, NULL, NULL),
(2, 25000, NULL, NULL),
(3, 30000, NULL, NULL),
(4, 50000, NULL, NULL),
(5, 75000, NULL, NULL),
(6, 100000, NULL, NULL),
(7, 150000, NULL, NULL),
(8, 200000, NULL, NULL),
(9, 300000, NULL, NULL),
(10, 500000, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `packet_data`
--

CREATE TABLE `packet_data` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `packet_id` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `packet_data`
--

INSERT INTO `packet_data` (`id`, `phone`, `packet_id`, `price`, `created_at`, `updated_at`) VALUES
(1, '+6289514223636', 2, 25000, '2020-10-03 01:48:26', '2020-10-03 01:48:26'),
(2, '+6289514223636', 2, 25000, '2020-10-03 02:48:15', '2020-10-03 02:48:15');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pulsas`
--

CREATE TABLE `pulsas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nominal_id` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pulsas`
--

INSERT INTO `pulsas` (`id`, `phone`, `nominal_id`, `price`, `created_at`, `updated_at`) VALUES
(1, '+6289514223636', 9, 30000, '2020-10-03 01:36:24', '2020-10-03 01:36:24'),
(2, '+6289514223636', 6, 200000, '2020-10-03 02:48:07', '2020-10-03 02:48:07');

-- --------------------------------------------------------

--
-- Table structure for table `p_l_n_prabayars`
--

CREATE TABLE `p_l_n_prabayars` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `customer_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nominal_id` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `p_l_n_prabayars`
--

INSERT INTO `p_l_n_prabayars` (`id`, `customer_id`, `nominal_id`, `price`, `created_at`, `updated_at`) VALUES
(1, '625367', 3, 30000, '2020-10-03 02:31:35', '2020-10-03 02:31:35'),
(2, '11114143', 8, 200000, '2020-10-03 02:36:49', '2020-10-03 02:36:49'),
(3, '625367', 2, 25000, '2020-10-03 02:48:24', '2020-10-03 02:48:24');

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`id`, `user_id`, `ip_address`, `user_agent`, `payload`, `last_activity`) VALUES
('0XOyMEvdIidqFtLVRGGSCTbhBaaVTb1iSxPxlZsd', 1, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'YTo2OntzOjY6Il90b2tlbiI7czo0MDoiNVpRcFBCQ0hEa05RT1VVVVBlUFJNWFNYQnJpdmN4M25CRGNJT29LWiI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6Mjc6Imh0dHA6Ly8xMjcuMC4wLjE6ODAwMC9wdWxzYSI7fXM6NjoiX2ZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fXM6NTA6ImxvZ2luX3dlYl81OWJhMzZhZGRjMmIyZjk0MDE1ODBmMDE0YzdmNThlYTRlMzA5ODlkIjtpOjE7czoxNzoicGFzc3dvcmRfaGFzaF93ZWIiO3M6NjA6IiQyeSQxMCRqdThXcmgzSzJVVC9nSVkzaFdQTG51bkdTVk5BU2c4Y3c4aG9OYUk5UTMxbVpTRlIvZ1ZtRyI7czoyMToicGFzc3dvcmRfaGFzaF9zYW5jdHVtIjtzOjYwOiIkMnkkMTAkanU4V3JoM0syVVQvZ0lZM2hXUExudW5HU1ZOQVNnOGN3OGhvTmFJOVEzMW1aU0ZSL2dWbUciO30=', 1601718756);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `two_factor_secret` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `two_factor_recovery_codes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `current_team_id` bigint(20) UNSIGNED DEFAULT NULL,
  `profile_photo_path` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `two_factor_secret`, `two_factor_recovery_codes`, `remember_token`, `current_team_id`, `profile_photo_path`, `created_at`, `updated_at`) VALUES
(1, 'Mochammad Annafia Oktafian', 'm.annafia28@gmail.com', NULL, '$2y$10$ju8Wrh3K2UT/gIY3hWPLnunGSVNASg8cw8hoNaI9Q31mZSFR/gVmG', NULL, NULL, NULL, NULL, NULL, '2020-10-02 12:20:23', '2020-10-02 12:20:23');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_packet_data`
--
ALTER TABLE `m_packet_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_pulsas`
--
ALTER TABLE `m_pulsas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_p_l_n_prabayars`
--
ALTER TABLE `m_p_l_n_prabayars`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `packet_data`
--
ALTER TABLE `packet_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `pulsas`
--
ALTER TABLE `pulsas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `p_l_n_prabayars`
--
ALTER TABLE `p_l_n_prabayars`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sessions_user_id_index` (`user_id`),
  ADD KEY `sessions_last_activity_index` (`last_activity`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `m_packet_data`
--
ALTER TABLE `m_packet_data`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `m_pulsas`
--
ALTER TABLE `m_pulsas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `m_p_l_n_prabayars`
--
ALTER TABLE `m_p_l_n_prabayars`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `packet_data`
--
ALTER TABLE `packet_data`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pulsas`
--
ALTER TABLE `pulsas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `p_l_n_prabayars`
--
ALTER TABLE `p_l_n_prabayars`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
