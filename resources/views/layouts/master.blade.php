<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>@yield('title')</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
        <link href="/css/app.css" rel="stylesheet">

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

        @yield('css')

        <link rel="shortcut icon" href="https://ecs7.tokopedia.net/img/favicon.ico?v=20140313">
    </head>
    <body class="antialiased">
        <nav class="navbar navbar-home">
            <div class="navbar-content">
                <a class="navbar-brand" href="/">
                    <img src="https://ecs7.tokopedia.net/assets/images/tokopedia-logo.png"  height="35" class="d-inline-block align-top" alt="" loading="lazy">
                </a>
                @if (Route::getCurrentRoute()->getName() != 'login')
                    <div class="float-right">
                        @if (!isset(Auth::user()->id))
                            <a href="/login" class="btn btn-outline-success my-2 my-sm-0">Masuk</a>
                        @else
                            <button type="button" class="btn rounded-circle">M</button>
                            <button type="button" class="btn rounded-circle">
                                <i class="fa fa-power-off"></i>
                            </button>
                        @endif
                    </div>
                @endif
            </div>
        </nav>

        @if (Route::getCurrentRoute()->getName() != 'login')
            <nav class="navbar menu-home bg-light">
                <div class="menu-content">
                    <a class="menu-item @if (Route::getCurrentRoute()->getName() == 'pulsa') active @endif" href="/pulsa">
                        <div>
                            <img
                                src="https://ecs7.tokopedia.net/img/cache/100-square/attachment/2020/8/28/47197032/47197032_914c9752-19e1-42b0-8181-91ef0629fd8a.png"
                                alt="Pulsa">
                        </div>
                        Pulsa
                    </a>
                    <a class="menu-item @if (Route::getCurrentRoute()->getName() == 'packet-data') active @endif" href="/packet-data">
                        <div>
                            <img
                                src="https://ecs7.tokopedia.net/img/cache/100-square/attachment/2019/10/22/21181130/21181130_907dac9a-c185-43d1-b459-2389f0b6efea.png"
                                alt="Paket Data">
                        </div>
                        Paket Data
                    </a>
                    <a class="menu-item @if (Route::getCurrentRoute()->getName() == 'pln') active @endif" href="/pln">
                        <div>
                            <img
                                src="https://ecs7.tokopedia.net/img/cache/100-square/attachment/2019/11/15/21181130/21181130_d593aa6d-5f53-4674-b1da-5988041541f4.png"
                                alt="PLN">
                        </div>
                        PLN Prabayar
                    </a>
                </div>
            </nav>
        @endif

        @yield('content')

        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>

        @yield('js')

        @yield('scripts')
    </body>
</html>
