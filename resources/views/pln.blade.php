@extends('layouts.master')

@section('title', 'PLN Prabayar | Tokopedia')

@section('content')
<div class="main-content">
    <div class="box-title">
        <span class="title">Beli PLN Prabayar</span>
    </div>

    @if (Session::has('pln_purchased'))
        <div class="alert alert-success" role="alert">
            {{Session::get('pln_purchased')}}
        </div>
    @endif

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form method="POST" action="{{ route('pln') }}">
        @csrf
        <div class="form-group">
            <label class="text-muted">No. Meter / ID Pelanggan</label>
            <input type="text" name="customer_id" class="form-control">
            <input type="hidden" name="nominal_id" class="form-control">
        </div>
        <div class="main-nominal">
            @foreach ($m_pln as $row)
                <div class="btn-nominal" data-action="choose-nominal" data-id="{{ $row->id }}">
                    <p class="nominal-text">Rp {{ number_format($row->nominal, 0) }}</p>
                </div>
            @endforeach
        </div>
        <div class="float-right mt-3">
            <button type="submit" class="btn btn-success">Beli Sekarang</button>
        </div>
    </form>
</div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $('[data-action="choose-nominal"]').click(function() {
                let id = $(this).data('id')
                $('[data-action="choose-nominal"]').removeClass('active')
                $('[name="nominal_id"]').val(id)
                $(this).addClass('active')
            })
        })
    </script>
@endsection
