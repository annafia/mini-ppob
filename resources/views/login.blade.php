@extends('layouts.master')
@section('title', 'Masuk / Login | Tokopedia')
@section('css')
    <link href="/css/login.css" rel="stylesheet">
@endsection
@section('content')
    <div class="main-login">
        <div class="login-box">
            <div class="login-container bg-white">
                <div class="title">
                    <h5 class="mb-4">
                        Masuk
                        <span class="float-right">
                            <a class="btn-register" href="#">Daftar</a>
                        </span>
                    </h5>
                </div>
                <form method="POST" action="{{ route('login') }}">
                    @csrf
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" name="email" class="form-control">
                        <small class="form-text text-muted">Contoh: example@tokopedia.com</small>
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" name="password" class="form-control">
                    </div>
                    <button type="submit" class="btn btn-block btn-secondary">Selanjutnya</button>
                </form>
            </div>
        </div>
    </div>
@endsection
