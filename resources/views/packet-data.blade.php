@extends('layouts.master')

@section('title', 'Paket Data | Tokopedia')

@section('content')
<div class="main-content">
    <div class="box-title">
        <span class="title">Beli Paket Data</span>
    </div>

    @if (Session::has('packet_data_purchased'))
        <div class="alert alert-success" role="alert">
            {{Session::get('packet_data_purchased')}}
        </div>
    @endif

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form method="POST" action="{{ route('packet-data') }}">
        @csrf
        <div class="form-group">
            <label class="text-muted">Nomor Telepon</label>
            <input type="text" name="phone" class="form-control">
            <input type="hidden" name="packet_id" class="form-control">
        </div>
        <div class="main-nominal">
            @foreach ($m_packet_data as $row)
                <div class="btn-nominal" data-action="choose-packet" data-id="{{ $row->id }}">
                    <p class="nominal-text">{{ $row->packet_name }}</p>
                    <p class="nominal-text">Rp {{ number_format($row->packet_price, 0) }}</p>
                </div>
            @endforeach
        </div>
        <div class="text-right mt-3">
            <button type="submit" class="btn btn-success">Beli Sekarang</button>
        </div>
    </form>
</div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $('[data-action="choose-packet"]').click(function() {
                let id = $(this).data('id')
                $('[data-action="choose-packet"]').removeClass('active')
                $('[name="packet_id"]').val(id)
                $(this).addClass('active')
            })
        })
    </script>
@endsection
