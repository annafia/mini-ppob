@extends('layouts.master')

@section('title', 'Pulsa | Tokopedia')

@section('css')
    <link href="/css/pulsa.css" rel="stylesheet">
@endsection

@section('content')
<div class="main-content">
    <div class="box-title">
        <span class="title">Beli Pulsa</span>
    </div>

    @if (Session::has('pulsa_purchased'))
        <div class="alert alert-success" role="alert">
            {{Session::get('pulsa_purchased')}}
        </div>
    @endif

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form method="POST" action="{{ route('pulsa') }}">
        @csrf
        <div class="form-group">
            <label class="text-muted">Nomor Telepon</label>
            <input type="text" name="phone" class="form-control" required>
            <input type="hidden" name="nominal_id" class="form-control" required>
        </div>
        <div class="main-nominal">
            @foreach ($m_pulsa as $row)
                <div data-action="choose-nominal" class="btn-nominal" data-id="{{ $row->id }}">
                    <p class="nominal-text">{{ number_format($row->nominal) }}</p>
                    <p class="nominal-text">Rp {{ number_format($row->price, 0) }}</p>
                </div>
            @endforeach
        </div>
        <div class="text-right mt-3 pr-2">
            <button type="submit" class="btn btn-success">Beli Sekarang</button>
        </div>
    </form>
</div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $('[data-action="choose-nominal"]').click(function() {
                let id = $(this).data('id')
                $('[data-action="choose-nominal"]').removeClass('active')
                $('[name="nominal_id"]').val(id)
                $(this).addClass('active')
            })
        })
    </script>
@endsection
