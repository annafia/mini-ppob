@extends('layouts.master')
@section('title', 'Tokopedia')
@section('content')
    <div class="main-content">
        <div id="main-slider" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img class="d-block w-100" height="240px" src="https://ecs7.tokopedia.net/img/banner/2020/8/27/42484317/42484317_848eb75b-36c9-4bef-a254-115418ae5638.jpg" alt="First slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" height="240px" src="https://ecs7.tokopedia.net/img/banner/2020/9/7/42484317/42484317_ffea4be4-2458-4347-8101-1cb8b118f7a2.jpg" alt="Second slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" height="240px" src="https://ecs7.tokopedia.net/img/banner/2020/6/24/42484317/42484317_e7e0a0ed-5dad-486e-a463-75a10818a6b4.jpg" alt="Third slide">
                </div>
            </div>
            <a class="carousel-control-prev" href="#main-slider" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#main-slider" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
        <div class="box-title">
            <span class="title">Berita Terkini</span>
        </div>
        <div class="main-news">
            <div class="news-item">
                <div class="news-img">
                    <img
                        width="200"
                        src="https://images.outbrainimg.com/transform/v3/eyJpdSI6IjE1ODMyM2ZkNTgyMGFhYTdjNGFhN2QzNzJkMzE0NDkzYWQxNzg4Y2UzZmU1ZDNkYWVkYzZhYTZiZjc1MGNmZTciLCJ3IjoxNzcsImgiOjExNywiZCI6MS41LCJjcyI6MCwiZiI6NH0.webp"
                        alt="">
                </div>
                <div class="news-info">
                    <h3 class="news-title">Bayar listrik 2 kali lebih sedikit dengan alat ini</h3>
                    <span>By Annafia</span>
                </div>
            </div>
            <div class="news-item">
                <div class="news-img">
                    <img
                        width="200"
                        src="https://asset.kompas.com/crops/cQ49hy5rrUajGfYCdZeVLFgqHYQ=/258x110:1075x654/177x117/data/photo/2020/10/02/5f772c23a7327.jpg"
                        alt="">
                </div>
                <div class="news-info">
                    <h3 class="news-title">Light Up Indonesia, 100.077 Keluarga Prasejahtera Terima Donasi Listrik</h3>
                    <span>By Annafia</span>
                </div>
            </div>
            <div class="news-item">
                <div class="news-img">
                    <img
                        width="200"
                        src="https://asset.kompas.com/crops/h52E3jme5rQQgQ1svF5sjaLU-ec=/13x0:892x586/177x117/data/photo/2019/07/09/1644953663.jpg"
                        alt="">
                </div>
                <div class="news-info">
                    <h3 class="news-title">Wapres AS Mike Pence Negatif Virus Corona, Langsung Dijauhkan dari Trump</h3>
                    <span>By Annafia</span>
                </div>
            </div>
        </div>
    </div>
@endsection
